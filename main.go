package main

import (
	"database/sql"
	"endpoint-with-db-docker/models"
	"github.com/gin-gonic/gin"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"net/http"
)

var BunDb *bun.DB

func main() {
	dsn := "postgres://postgres:helloworld@psql_db:5432/test?sslmode=disable"
	DBCon := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))
	BunDb = bun.NewDB(DBCon, pgdialect.New())
	defer BunDb.Close()
	//_, err := BunDb.NewCreateTable().Model((*models.User)(nil)).Exec(context.Background())
	//_, err := BunDb.NewInsert().Model(&models.User{ID: 1, Name: "Jane Doe"}).Exec(context.Background())
	//if err != nil {
	//	panic(err)
	//}
	router := gin.Default()

	router.GET("/users/:id", GetUser)

	router.Run("0.0.0.0:8080")

}
func GetUser(c *gin.Context) {
	id := c.Param("id")
	var user models.User

	err := BunDb.NewSelect().Model(&user).Where("id = ?", id).Scan(c)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"Internal Server Error": err.Error()})
		return
	}

	//file, err := os.OpenFile(
	//	"demo.txt",
	//	os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
	//	0666)
	//if err != nil {
	//	log.Fatal(err)
	//}
	//defer file.Close()
	//
	//byteSlice, err := json.Marshal(user)
	//bytesWritten, err := file.Write(byteSlice)
	//
	//if err != nil {
	//	log.Fatal(err)
	//}
	//log.Printf("Wrote %d bytes.\n", bytesWritten)

	c.JSON(http.StatusOK, user)
}
