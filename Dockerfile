FROM golang:1.22.3-alpine AS build

WORKDIR /app

COPY . .

RUN go mod download

RUN go build -o /godocker

FROM alpine:3.20
COPY --from=build /godocker /godocker
ENTRYPOINT [ "/godocker" ]