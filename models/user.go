package models

type User struct {
	ID   int    `bun:",pk"`
	Name string `bun:",notnull"`
}
